#ifndef OFD_TOPOLOGY_HH_
#define OFD_TOPOLOGY_HH_

#include <iostream>
#include <fluid/of10msg.hh>
#include <fluid/of13msg.hh>
#include <fluid/of10/of10common.hh>
#include <fluid/of13/of13common.hh>
#include <array>
#include <set>

namespace topology{

using namespace fluid_msg;


class node {
private:    
	 uint64_t _node_id;
	 std::array<char,16> _addr;
	 std::vector<of10::Port> _ports;
	 std::vector<of13::Port> _ports13;
public:
	 node(uint64_t _nid,std::vector<of10::Port> ports ):_node_id(_nid),_ports(ports)
	 {
	 }
	 
         uint64_t node_id(){
	    return this->_node_id;
	 }
         EthAddress getPortHWAddress(uint16_t _port_id)
         {
	     for (std::vector<of13::Port>::iterator it = _ports13.begin() ; it != _ports13.end(); ++it)
                  if(it->port_no()== _port_id)
			 return it->hw_addr();
	}

	 uint64_t get_node_id()
         {
            return this->_node_id;
	 }
	 
};
	 
class link
{
   uint64_t  _src_node_id;
   uint16_t  _src_node_port_id;
   uint64_t  _dst_node_id;
   uint16_t  _dst_node_port_id;
  
  public:  
   enum LinkType 
   {
   INVALID_LINK,
   DIRECT_LINK,
   MULTIHOP_LINK,
   TUNNEL
   };
   
 private:
   LinkType _linkType;  
  
  public:
   uint64_t src_node_id(){
    return this->_src_node_id;
   }
   uint16_t src_node_port_id(){
      return this->_src_node_port_id;
   }
   uint64_t dst_node_id(){
     return this->_dst_node_id;
   }
   uint16_t dst_node_port_id(){
      return this->_dst_node_port_id;
   }
   void src_node_id(uint64_t src_node_id){
      this->_src_node_id=src_node_id;
   }
   void src_node_port_id(uint16_t src_node_port_id){
      this->_src_node_port_id=src_node_port_id;
   }
   void dst_node_id(uint64_t dst_node_id){
      this->_dst_node_id=dst_node_id;
   }
   void dst_node_port_id(uint16_t dst_node_port_id){
      this->_dst_node_port_id=dst_node_port_id;
   }   

   link(uint64_t node_id, uint64_t src_port_id, uint64_t node_id_dst, uint64_t dst_port_id, LinkType _linkType):_src_node_id(node_id),_src_node_port_id(src_port_id),_dst_node_id(node_id_dst),_dst_node_port_id(_dst_node_port_id),_linkType(_linkType)
   {
     //object created. 
   }
 };


class topology{
    static std::set<node> _switchPorts;
    static std::set<link> _links;
	//May be used:
	//protected Map<DatapathId, Set<Link>> switchLinks;
	
   topology()
   {

   }
    	
  static void add_node(node _node)
  {
    _switchPorts.insert(_node);
  }
  
  static  node get_node(uint64_t _node_id)
  {
	std::set<node>::iterator iter = _switchPorts.begin();
        //node _node;		   
        while(iter != _switchPorts.end())
        {
          node  _node = *iter;
	  if (_node.get_node_id()==_node_id)
              return _node;
			
           iter++;
			
        }
       
  }
  static void add_port(node *_node, uint8_t portid, uint64_t mac_addr)
	{
	}
  
  static void add_link(uint64_t node_id, uint16_t port_id, uint64_t node_id_dst, uint16_t dst_node_port_id, link::LinkType _linkType )
	{
	   link _link(node_id, port_id,node_id_dst, dst_node_port_id, _linkType);
          _links.insert(_link);
  	   
	}

static bool check_link(link is_new_link)
	{
	   std::set<link>::iterator iter = _links.begin();
		   
           while(iter != _links.end())
           {
            link _link = *iter;
			if (_link.src_node_id()==is_new_link.src_node_id() && _link.src_node_port_id()==is_new_link.src_node_port_id() && _link.dst_node_id()==is_new_link.dst_node_id() && _link.dst_node_port_id()==is_new_link.dst_node_port_id() )
                return true;
			
            iter++;
			
          }
		  
		  return false;
	}
    static bool delete_port();
	
	
    static bool delete_node();

	
    static bool delete_link();

	

};

}

#endif /* OFD_TOPOLOGY_HH */





  	




  
 




