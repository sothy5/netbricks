#include <iostream>
#include <stdint.h>
#include <list> 

namespace topology{

    struct LLDPTLV {
          unsigned char type;
          uint8_t length;
          unsigned char value[];
  	
 };

class LLDP
{
  private:
   LLDPTLV  _chassisId;
   LLDPTLV  _portId;
   LLDPTLV  _ttl;
   std::list<LLDPTLV> _optionalTLVList;
   uint8_t _ethType;
  
  public: 
          LLDP(LLDPTLV chassisId, LLDPTLV portid,LLDPTLV ttl);
          virtual ~LLDP();
          
          bool operator==(const LLDP &other) const;
          bool operator!=(const LLDP &other) const;
          
	  LLDPTLV chassisId() {
            return this->_chassisId;
          }
          LLDPTLV portId() {
            return this->_portId;
          }
          LLDPTLV ttl() {
            return this->_ttl;
          }
         std::list<LLDPTLV> optionalTLVList() {
            return this->_optionalTLVList;
         }
         uint8_t ethType() {
             return this->_ethType;
         }
         
        void chassidId(LLDPTLV chassisId) {
            this->_chassisId = chassisId;
         }
        void portId(LLDPTLV portId) {
           this->_portId = portId;
        }
        void ttl(LLDPTLV ttl) {
           this->_ttl = ttl;
        }
       void optionalTLVList(std::list<LLDPTLV> optionalTLVList) {
            this->_optionalTLVList = optionalTLVList;
        }
		
	void addoptionalTLVList(LLDPTLV lldpTlv)
	{
	    this->_optionalTLVList.push_back(lldpTlv);
	}
        void ethType(uint8_t ethType) {
            this->_ethType = ethType;
        }

       uint8_t* pack();
       uint16_t getLength();
       bool unpack(uint8_t *buffer);
		
 };

}

