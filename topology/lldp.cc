#include "lldp.hh"
#include "fluid/util/util.h"
#include <iterator>

namespace topology{

 

uint8_t* LLDP::pack()
	{
		   uint8_t* buffer;
                   memcpy(buffer,&(this->_chassisId), this->_chassisId.length);
		   memcpy(buffer+(this->_chassisId.length),&(this->_portId),this->_portId.length);
		   memcpy(buffer+(this->_portId.length+this->_chassisId.length), &(this->_ttl),this->_ttl.length);
		   
		   std::list<LLDPTLV>::iterator iter =this->_optionalTLVList.begin();
		   uint8_t len=this->_portId.length+this->_chassisId.length+this->_ttl.length;
                   
                   while(iter != this->_optionalTLVList.end())
                   {
                     struct LLDPTLV s = *iter;
                     
                     memcpy(buffer+len,&s,s.length);
                     len+=s.length;
                     iter++;
		     
                   }
		  return buffer; 
		   
	}

uint16_t LLDP::getLength()
{
        uint16_t len=this->_chassisId.length+this->_portId.length+this->_ttl.length;
        std::list<LLDPTLV>::iterator iter = this->_optionalTLVList.begin();
        while(iter != this->_optionalTLVList.end())
           {
            struct LLDPTLV s =*iter;
	    len+=s.length;
            iter++;
            
	   }
	return len;
}

bool LLDP::unpack(uint8_t *buffer)
{
	this->_chassisId.type=(uint8_t)*((uint8_t *)(buffer));
        this->_chassisId.length= (uint8_t)*((uint8_t *)(buffer+1));
		  
	if (this->_chassisId.length==1)
           this->_chassisId.value[0]=(uint8_t)(*(uint8_t *)(buffer+2));
        else if (this->_chassisId.length==2)
           memcpy(&(this->_chassisId.value),(uint16_t *)(buffer+2),2);
        else if (this->_chassisId.length==4)	
           memcpy(&(this->_chassisId.value), (uint32_t *)(buffer+2),4);
        else if (this->_chassisId.length==8)	
           memcpy(&(this->_chassisId.value),(uint64_t *)(buffer+2),8);	
          
          
        this->_portId.type= (uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length));
        this->_portId.length=(uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+1));
		  
        if (this->_portId.length==1)
            this->_portId.value[0] =(uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+2));
        else if (this->_portId.length==2)
            memcpy(&(this->_portId.value),(uint16_t *)(buffer+2+this->_chassisId.length+2),2);
        else if (this->_portId.length==4)	
            memcpy(&(this->_portId.value),(uint32_t *)(buffer+2+this->_chassisId.length+2),4);
        else if (this->_portId.length==8)	
            memcpy(&(this->_portId.value),(uint64_t *)(buffer+2+this->_chassisId.length+2),8);

        this->_ttl.type=(uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length));
        this->_ttl.length= (uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+1));
		  
         if (this->_ttl.length==1)
	     this->_ttl.value [0]= (uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+2));
         else if (this->_ttl.length==2)
	    memcpy(&(this->_ttl.value),(uint16_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+2),2);
         else if (this->_ttl.length==4)	
            memcpy(&(this->_ttl.value),(uint32_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+2),4);
         else if (this->_ttl.length==8)	
            memcpy(&(this->_ttl.value),(uint64_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+2),8);			   
		  
  }

}
