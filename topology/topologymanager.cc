#include "topologymanager.hh"

namespace topology{
	
	
	 OFMsg topology_manager::packetOut_generation(node _node, uint16_t port_id,bool isStandard, bool isReverse)
	 {
		        _node.node_id();
			  
			struct LLDPTLV chassisId;
			chassisId.type=1;
			chassidId.length=_node.node_id().length();
			memcpy(&chassidId.value,&_node.node_id(),_node.node_id().length());
					     
			struct LLDPTLV portId;
			portId.type=2;
			portId.length=port_id.length();
			memcpy(&portId.value,&port_id,port_id.length());
			 
			char []ttlValue = { 0, 0x78 };
			struct LLDPTLV ttl;
			ttl.type=3;
			ttl.length=ttlValue.length();
			ttl.value=ttlValue;
			 
			LLDP lldp = new LLDP(chassisId,portId,ttl);
		        // OpenFlow OUI - 00-26-E1-00
		        unsigned char [] dpidTLVValue = { 0x0, 0x26, (byte) 0xe1, 0, 0, 0,
				0, 0, 0, 0, 0, 0 };
		        LLDPTLV dpidTLV ; 
			dpidTLV.type= 127;
			dpidTLV.length=dpidTLV.length();
			dpidTLV.value=dpidTLVValue;
							
		       lldp.addoptionalTLVList(dpidTLV);
			 
			
	  	        LLDPTLV forwardTLV;
			forwardTLV.type=TLV_DIRECTION_TYPE;
			forwardTLV.length=TLV_DIRECTION_LENGTH;
			forwardTLV.value=TLV_DIRECTION_VALUE_FORWARD;
	
			LLDPTLV reverseTLV;
			reverseTLV.type=TLV_DIRECTION_TYPE;
			reverseTLV.length=TLV_DIRECTION_LENGTH;
			reverseTLV.value=TLV_DIRECTION_VALUE_REVERSE;
	
			if (isReverse) {
					lldp.addoptionalTLVList(reverseTLV);
				} else {
					lldp.addoptionalTLVList(forwardTLV);
		    }
	
			Ethernet ethernet;
			
			if (isStandard) 
			{
				unsigned char []LLDP_STANDARD_DST_MAC_STRING= {01,80,c2,00,00,0e};
				unsinged char LLDP_EtherType={ 0x88CC}; 
				
				uint8_t* buf;
				
				memcpy(buf,&LLDP_STANDARD_DST_MAC_STRING,6);
				memcpy(buf+6,getPortHWAddress(uint16_t _port_id),6)
				mempcpy(buf+12,&LLDP_EtherType,2);
				memcpy(buf+14,lldp.pack(),getLength())
				
				of13::PacketOut po(pi.xid(), -1, OFPP_CONTROLLER);
				po.data(pi.data(), pi.data_len());
				of13::OutputAction act(port_id, 1024); // = new of13::OutputAction();
				po.add_action(act);
				return po;
				

				 
			 }
	 }
	 
	 bool topology_manager::packetOut_forward(node _node,OFmsg ofmsg)
	 {
		 
	 }
	 void topology_manager::packetIn_process(node incoming_node, OFMsg ofmsg)
	 {
		//node create if not exist
		//link create if not exist
		//link update if link there and does not match
		if (ofmsg.getType()==PACKET_IN)
                {
			of10::Port _port10;
			of13::Port _port13;
				 
	                if (ofmsg.getVersion()<2)
                         {
				uint16_t inport=ofmsg.in_port();
				const uint8_t * _dataPtr=ofmsg.data();
				
				this->handle_LLDPMsg(this-> processLLDPMsg(_dataPtr),incoming_node,inport);
				
			}
                        else
                        {
				of13::Inport *port=ofmsg.match().in_port();
		        }				 
                 				 
			  		
		 
		 
		} 
	 }
	 
         LLDP topology_manager::processLLDPMsg(const uint8_t* data )
		{
		  uint16_t ethType=ntoh16((uint16_t*) (data+12) ); 
                  if (ethType==LLDP_EtherType)
                  {
		     LLDP _lldp;
			 _lldp.unpack(data+14);
			 return _lldp;
			  
		   }		  
		}
		
	void  topology_manager::handle_LLDPMsg(LLDP lldp, node incoming_node,uint16_t inport)
		{
			uint64_t myId=0;
			uint64_t other_id=0;
			bool my_lldp=false;
			bool is_reverse=null;
			
			if (lldp.portId()!=null && lldp.portId().length==3)
			{
				uint16_t remote_port=lldp.portId.value;
				node remote_node=null;
				
				for (LLDPTL lldptlv: lldp.optionalTLVList()){
					if (lldptlv.type()==127 && lldptlv.length()==12 && lldptlv.value()[0]==0x0 && lldptlv.value()[1]==0x26 && lldptlv.value()[2]==(byte)0xe1 && lldptlv.value()[3]==0x0 )
					{
						uint64_t node_id=lldptlv.value();
						remote_node= _topology.get_node(node_id);
					}else if(lldptlv.type()==127 && lldptlv.length()==12 && lldptlv.value()[0]==0x0 && lldptlv.value()[1]==0x26 && lldptlv.value()[2]==(byte)0xe1 && lldptlv.value()[3]==0x1)
					{
						//timestamp issue.
					}else if(lldptlv.type()==12 && lldptlv.length()==8)
					{
					   uint64_t=(uint64_t)lldptlv.value;
                        // check for same 					   
					}else if (lldptlv.type==TLV_DIRECTION_TYPE  &&lldptlv.length== TLV_DIRECTION_LENGTH){
						if((lldptlv.value)[0]== TLV_DIRECTION_VALUE_FORWARD[0])
						   is_reverse=false;
                        else if(lldptlv.value)[0]== TLV_DIRECTION_VALUE_REVERSE[0]) 
                            is_reverse=true;							
						
					}
					
				}
			}
			
			if(my_lldp==false)
			{
				//not send from controller
			}
			
			//link found_link= new link(incoming_node.node_id(),inport,remote_node.node_id(),remote_port);
			
			this._topology.add_link(incoming_node.node_id(),inport,remote_node.node_id(),remote_port);
			
			
		}
		
		static topology_manager::feature_response_process(OFMsg ofmsg){
              // OF feature_response msg.
			  //  create a new node if not exist and add port details.
			  
			  if (ofmsg.getVersion >0 && ofmsg.getVersion < 4)
			  {
				  FeaturesReply featuresReply=(FeaturesReply)ofmsg;
				  node _node(featuresreply.datapath_id(),featureReply.ports());
				  _topology.insert(_node);
			  }				  
          		
		}
		
	//helper class	
	void  set_controller_tlv(){
		uint32_t prime=7867;
		unsigned char[] controller_tlv_value={0,0,0,0,0,0,0,0};
		struct timespec ts;
		timespec(&ts,TIME_UTC);
		
		uint64_t intermediate_value=ts.tv_nsec;
		
		std::size_t ip_hash=boost::hash_value(192168117);
		intermediate_value=intermediate_value*prime+ip_hash;
		memcpy(&controller_tlv_value,&intermediate_value,sizeof(intermediate_value));
		this.controller_tlv.type=0xc;
		this.controller_tlv.length=controller_tlv_value.length;
		this.controller_tlv.value=controller_tlv_value;
			
		}
}
