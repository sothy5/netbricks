
#include "lldp.hh"
#include "topology.hh"
#include <fluid/of10msg.hh>
#include <array>

namespace topology{


class topology_manager

{
  static topology _topology;
  const uint16_t LLDP_EtherType= 0x88CC; 
  
  const unsigned char TLV_DIRECTION_TYPE = 0x73;   
  const int8_t TLV_DIRECTION_LENGTH = 1; // 1 byte
  const unsigned char TLV_DIRECTION_VALUE_FORWARD = 0x01 ;
  const unsigned char TLV_DIRECTION_VALUE_REVERSE = 0x02 ;
  
  LLDPTLV controller_tlv;
	
	
   public:         
    OFMsg  packetOut_generation(node _node, uint16_t port_id,bool isStandard, bool isReverse);
    bool   packetOut_forward(node outgoing_node, OFMsg ofmsg);
    static void packetIn_process(node incoming_node, OFMsg ofmsg);
    static bool switch_up_event(node add_node);
    static bool switch_down_event(node del_node);
    static bool feature_response_process(OFMsg ofmsg);
    static bool mulitpart_response_process(OFMsg ofmsg);
	
    LLDP    process_LLDPMsg(const uint8_t* data );
    void    handle_LLDPMsg(LLDP lldp, node incoming_node, uint16_t port_id);
	   
	   
	   
	   
};

}
