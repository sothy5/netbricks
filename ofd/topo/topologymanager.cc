#include "../ofserver.hh"
#include "topologymanager.hh"
#include "net/ethernet.hh"
#include <iostream>
#include <sstream>
#include "protocols.hh"
#include "data.hh"
#include "tlv.hh"
#include "lldp_packet.hh"
#include "core/sleep.hh"


namespace ofd{
	
future<> topology_manager::read_queue_msg(wrap_msg fr)
{
        
     of10::FeaturesReply featuresReply;
     
     if(fr._msgType== fluid_msg::of10::OFPT_FEATURES_REPLY) 
     {
         featuresReply.unpack((uint8_t *)(fr._buf.data()));          
         std::vector<std::unique_ptr<of10::PacketOut>>  vec_packet_out=feature_response_process(std::move(featuresReply));
         for (unsigned i=0; i<vec_packet_out.size() ; i++)
         {
           std::vector<uint8_t> vec={};
           std::unique_ptr<of10::PacketOut> po=std::move(vec_packet_out[i]);
           uint8_t *vec_data=po->pack();
           vec.assign(vec_data, vec_data+po->length());
           wrap_msg_return _wmr(fr._dpid,of10::OFPT_PACKET_OUT, vec);     
        
           this->_server._spsc_queue_topo_ofd->push_eventually(std::move(_wmr)).then([]{
                          return  make_ready_future<>();
        
                    });
          
          vec.clear();
          po.reset();     

       
         }
         vec_packet_out.clear();
     } else if (fr._msgType== fluid_msg::of10::OFPT_PACKET_IN)
     {
           std::cout<<"Came for topology" << std::endl;
           of10::PacketIn packetIn;
           packetIn.unpack((uint8_t *)(fr._buf.data()));
           packetIn_process(fr._dpid,fr._connection_id,std::move(packetIn));
           
           
   
     } else if (fr._msgType == fluid_msg::of10::OFPT_PORT_STATUS)
     {
   
     }       
    
     return make_ready_future<>(); 
     
}




future<> topology_manager::process_Msg()
{
 
  return when_all(read(), verify()).then([] (std::tuple<future<>,future<>> joined) {
           std::get<0>(joined).ignore_ready_future();
           std::get<1>(joined).ignore_ready_future();
   });
}

future<> topology_manager::read() {
         return  keep_doing([this]{
            
            return this->_server._spsc_queue_topo->pop_eventually().then_wrapped([this] (auto f) {
                  try
                    {
                       wrap_msg wm =f.get0();

                       return this->read_queue_msg( std::move(wm)).then([]() {
                         //std::cout << "features reply processed"<< std::endl;
                              return make_ready_future<>();
                       });


                        return make_ready_future<>();

                    }catch(...)
                    {
                        std::cerr<< "request error" <<std::current_exception()<< std::endl;
                        return make_ready_future<>();
                    }

           });
            
                                
            });  
         

}

future<> topology_manager::verify(){
         return sleep(60s).then([this] {
              return validate_topology();
     
      });
} 

of10::PacketOut topology_manager::packetOut_generation(node _node, uint16_t port_id, bool isStandard, bool isReverse)
{

// https://github.com/wallnerryan/floodlight/blob/master/src/main/java/net/floodlightcontroller/linkdiscovery/internal/LinkDiscoveryManager.java

  //if(isStandard)

 
   of10::PacketOut po;   
   //std::cout<< "Node id"<<_node.node_id()<< std::endl;
   if ( _node.node_id()==0){
          return po;
    }
     uint8_t* srcAdd=_node.getPortHWAddress(port_id).get_data();
     char pid=port_id+'0';
     uint8_t portid= static_cast<int>(pid);
    
     struct LLDPTLV portId;
            portId.type=2<<1;
            portId.length=2;
            portId.value.push_back(7);
            portId.value.push_back(portid);

     
      unsigned char ttlValue [] = { 0, 0x78 };
           struct LLDPTLV ttl;
           ttl.type=3<<1;
           ttl.length=sizeof(ttlValue);
           ttl.value.push_back(ttlValue[0]);
           ttl.value.push_back(ttlValue[1]);
      struct LLDPTLV chassisIdTLV; 
                     chassisIdTLV.type=1<<1;
                     chassisIdTLV.length=7;
      uint64_t datapath_id= hton64(_node.node_id());
      uint8_t result[sizeof(datapath_id)];
      std::memcpy(result, &datapath_id, sizeof(datapath_id));      
      
      chassisIdTLV.value.push_back(4);
      chassisIdTLV.value.push_back(result[2]);
      chassisIdTLV.value.push_back(result[3]);
      chassisIdTLV.value.push_back(result[4]);
      chassisIdTLV.value.push_back(result[5]);
      chassisIdTLV.value.push_back(result[6]);
      chassisIdTLV.value.push_back(result[7]);
     
     
      struct LLDPTLV systemName;
                     systemName.type=5<<1;
                     systemName.length=10;
      
     
      std::stringstream ss;
      ss << "openflow:" << _node.node_id();
      systemName.length=ss.str().length();
      std::string _ssystemName=  ss.str();
     

      for ( std::string::iterator it=_ssystemName.begin(); it!=_ssystemName.end(); ++it)
            systemName.value.push_back(*it);
   
  
      LLDP lldp(chassisIdTLV,portId,ttl,systemName);
  
                        struct LLDPTLV forwardTLV;
                        forwardTLV.type=TLV_DIRECTION_TYPE;
                        forwardTLV.length=TLV_DIRECTION_LENGTH;
                        forwardTLV.value.push_back(TLV_DIRECTION_VALUE_FORWARD);
                        //memcpy(&forwardTLV.value,&TLV_DIRECTION_VALUE_FORWARD, TLV_DIRECTION_LENGTH);

                        struct LLDPTLV reverseTLV;
                        reverseTLV.type=TLV_DIRECTION_TYPE;
                        reverseTLV.length=TLV_DIRECTION_LENGTH;
                        reverseTLV.value.push_back(TLV_DIRECTION_VALUE_REVERSE );
 
                           
                          
                      
                        if (isStandard)
                        {
                                uint8_t LLDP_STANDARD_DST_MAC_STRING[] ={0x01,0x23,0x00,0x00, 0x00, 0x01};
                                uint8_t LLDP_EtherType []={0x88,0xCC}; 
                               
                                std::vector<uint8_t> buf={};
                                buf.insert(buf.begin(), LLDP_STANDARD_DST_MAC_STRING,LLDP_STANDARD_DST_MAC_STRING+6);
                          
                               
                                buf.insert(buf.begin()+6, srcAdd,srcAdd+6);
                                buf.insert(buf.begin()+12, LLDP_EtherType, LLDP_EtherType+2);
                               
                                
                                std::vector<uint8_t> lldpData={};
                                lldpData = lldp.pack(); 


                                buf.insert(buf.begin()+14, lldpData.begin(), lldpData.end());
                                
                                po.xid(1);
                                po.buffer_id(OFP_NO_BUFFER);
                                po.in_port(of10::OFPP_CONTROLLER);
                                of10::OutputAction act(port_id, 1024); // = new of13::OutputAction();
                                po.add_action(act);
                                //std::cout<<"buf size" << 14+lldpData.size() << std::endl; 
				po.data(buf.data(), 14+lldpData.size());
                                buf.clear();  
                                
                                return po;



                         }


                      return po;

    


}

    std::list<node> discovered_topology::switchPorts;          
    
    
    std::vector<std::unique_ptr<of10::PacketOut>> topology_manager::feature_response_process(of10::FeaturesReply _featuresReply) 
    {
      
     node _node =discovered_topology::get_node( _featuresReply.datapath_id());
 
      if(_node.node_id()==0)
      {
          _node.node_id(_featuresReply.datapath_id());
          _node.ports(_featuresReply.ports());
          discovered_topology::add_node(_node);
          std::cout<< "Add Node with ID" << _node.node_id()<< std::endl;
      }
      
           std::vector<std::unique_ptr<of10::PacketOut>> vec={} ;
           std::cout <<"Port counter"<<_featuresReply.ports().size()<< std::endl;
                 
            for (of10::Port _port : _featuresReply.ports())
            {
              if(_port.config()!= 0x00000001  && _port.state() != 0x00000001 && _port.port_no() < 255  )
              {
                 //std::cout<<"Port No packet out message" << _port.port_no()<< std::endl;
                 of10::PacketOut p1 = topology_manager::packetOut_generation(_node,_port.port_no(),true,false);
                 std::unique_ptr<of10::PacketOut> ptr=std::make_unique<of10::PacketOut>(p1); 
                 vec.push_back(std::move(ptr));
                 ptr.reset();           
             
              }
              
               //std::cout <<"PACKET DOES NOT COME"<< std::endl;
 
            }
            
          return vec; 
      
    }
   
  future<> topology_manager::validate_topology(){
   
     
           if(!discovered_topology::switchPorts.empty()){
              std::list<node>::iterator iter= discovered_topology::switchPorts.begin();
              node _node;
              while (iter != discovered_topology::switchPorts.end()){
                _node=*iter;
                
                for(of10::Port _port :_node.ports()) {
                   std::vector<uint8_t> vec={};              
                   of10::PacketOut p1 = topology_manager::packetOut_generation(_node,_port.port_no(),true,false);
                   //std::unique_ptr<of10::PacketOut> ptr=std::make_unique<of10::PacketOut>(p1);
                   //_vec.push_back(std::move(ptr));
                    
                    uint8_t *vec_data=p1.pack();
                    vec.assign(vec_data, vec_data+p1.length());
                    wrap_msg_return _wmr(_node.node_id(),of10::OFPT_PACKET_OUT, vec);

                    this->_server._spsc_queue_topo_ofd->push_eventually(std::move(_wmr)).then([]{
                          return  make_ready_future<>();

                    });

                 delete (vec_data);
                 
                } 
                iter++;
              }
     }
    return make_ready_future<>();
       
  }  
    
   std::list<link> discovered_topology::links;
 
   void topology_manager::packetIn_process(uint64_t  incoming_node_id, uint32_t connection_id, of10::PacketIn packetIn){
       //create a link .  Incoming node_id, port_id
       //LLDP src node_id, port_id
       //create link
       
       node _node =discovered_topology::get_node( incoming_node_id);

       if(_node.node_id()!=0)
       {
       
         uint16_t in_port= packetIn.in_port();
                         
         Data _data;
         _data.data=((uint8_t *)packetIn.data())+14;
         _data.length =packetIn.data_len()-14;

	 LLDPPacket *_lldpPacket =new LLDPPacket(_data, Packet::Protocols() );
        
        string mac_address;
        int port_id_remote; 
         
                  
         TLVs tlvs = _lldpPacket->readPacket();
         TLVs::iterator it; 

         for (it = tlvs.begin(); it != tlvs.end(); ++it) {
           
            if ((*it)->getSubTypeName().length()) {     // subtype is set, print type name together with subtype name + value
                if ((*it)->tlv_type==1 && (*it)->getSubType()==4 )  
                                string mac_address=(*it)->getValueStr();
                if ((*it)->tlv_type==2 && (*it)->getSubType()==7 )
                               port_id_remote= std::stoi( (*it)->getValueStr());

              
                std::cout << "\t" <<  (*it)->getTypeName() << " (" << (*it)->getSubTypeName() << "): " << (*it)->getValueStr() << std::endl;
             } else {                                    // subtype not set, print only type name + value
               std::cout << "\t" << (*it)->getTypeName() << ": " << (*it)->getValueStr() << std::endl;
             }  
         }
         delete (_lldpPacket);
   
    
         node _node1=discovered_topology::get_node(mac_address);
            if (_node.node_id()!=0){
               
               link _link (_node1.node_id(),port_id_remote,_node.node_id(),in_port,link::LinkType::DIRECT_LINK );
               discovered_topology::add_link(_link);
               std::cout<<"Linked added"<< _node1.node_id() <<":"<< port_id_remote <<":" <<_node.node_id() << ":" << in_port << std::endl;  
        
            }
         
        
       }
         
       std::cout<<"GONE"<<std::endl;
         

                
       
   }   

 

   struct node_id_port_id{
       uint16_t _port_id;
       uint64_t _node_id;  
   };

   	
      
  future<> topology_manager::of_portstatus( uint64_t  dpid, std::unique_ptr<of10::PortStatus> ps){
        
        
        std::cout<<"of port status name"<<ps->desc().name()<< std::endl;
        
        std::cout<<"of port status config" <<ps->desc().config() << std::endl;
        std::cout<<"of port status state"<<ps->desc().state() << std::endl;
        std::cout<<"of port status curr"<<ps->desc().curr() << std::endl;
        std::cout<<"of port  status advertised"<< ps->desc().advertised()<< std::endl;
        std::cout <<"of port status supported" << ps->desc().supported()<< std::endl;
        std::cout << "of port status peer" << ps->desc().peer() << std::endl;
       
      //  node _node =discovered_topology::get_node(dpid);

       
        if(ps->reason()==00)
        {
          std::cout<<"add port"<< std::endl; ;
          //ADD
        }else if (ps->reason()==1)
        {
          std::cout<<"delete port"<< std::endl;
          //DELETE
        }else if (ps->reason()==2)
        {
           std::cout<< "modify"<< std::endl;
          //MODIFY
        }
        
          return make_ready_future<>();

        // ADD
       //delete
        //modify
     }
   }
	
    
