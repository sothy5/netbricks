#include "lldp.hh"
#include "fluid/util/util.h"
#include <iterator>
#include <sstream> 
namespace ofd{

 

std::vector<uint8_t> LLDP::pack()
	{
		   //uint8_t *buffer=new uint8_t[this->getLength()]; 
                   //buffer[0]=this->_chassisId.type;
                   //buffer[1]=this->_chassisId.length;
                   //memcpy(buffer,&(this->_chassisId.value), sizeof(this-> _chassisId));
                   //std::cout << "arrive here" << std::endl;                  
                   std::vector<uint8_t> buffer={};;
                   buffer.push_back(this->_chassisId.type);
                   buffer.push_back( this->_chassisId.length);
                   std::vector<uint8_t>::iterator  it=buffer.end();
                   buffer.insert(it, this->_chassisId.value.begin(), this->_chassisId.value.end());   


                   buffer.push_back(this->_portId.type);
                   buffer.push_back(this->_portId.length);
                   std::vector<uint8_t>::iterator  it1=buffer.end();
                   buffer.insert(it1, this->_portId.value.begin(), this->_portId.value.end());
                   
                   buffer.push_back(this->_ttl.type);
                   buffer.push_back(this->_ttl.length);
                   std::vector<uint8_t>::iterator  it2=buffer.end();
                   buffer.insert(it2, this->_ttl.value.begin(), this->_ttl.value.end());
    
                   //buffer[2+this->_chassisId.length]=this->_portId.type;
                   //buffer[2+this->_chassisId.length+1]=this->_portId.length;
		   //memcpy(buffer+sizeof(this->_chassisId) ,&(this->_portId),sizeof(this->_portId));
                   //buffer[2+this->_chassisId.length+2+this->_portId.length]=this->_portId.type;
                   //buffer[2+this->_chassisId.length+2+this->_portId.length+1]=this->_portId.length;
                   //memcpy(buffer+sizeof(this->_chassisId)+sizeof(this->_portId), &(this->_ttl),sizeof(this->_ttl));
                   /*                 
  
                   buffer= static_cast<uint8_t *>(&this->_chassisId);
                   buffer[2+this->_chassisId.length]= static_cast<uint8_t *> (&this->_portId);
                   buffer[2+this->_chassisId.length+2+this->_portId]=static_cast<uint8_t *> (&this->_ttl);  
		   std::cout<< "size of chasisId" << sizeof(this->_chassisId) << std::endl;
                   std::cout<< "size of portid" << sizeof(this->_portId) << std::endl;
                   std::cout<< "size of ttl" << sizeof(this->_ttl) << std::endl;
                   */
                   buffer.push_back(this->_systemName.type);
                   buffer.push_back(this->_systemName.length);
                   std::vector<uint8_t>::iterator  it3=buffer.end();
                   buffer.insert(it3, this->_systemName.value.begin(), this->_systemName.value.end());

                     
                  
                   std::list<LLDPTLV>::iterator iter =this->_optionalTLVList.begin();
		   //uint8_t len = 2 + this->_portId.length + 2 + this->_chassisId.length + 2 + this->_ttl.length+2+this->_systemName.length;
                   //std::cout<< "Data length" << len << std::endl;
                    
                   /* 
                   std::ostringstream ss1;
                   std::copy(buffer.begin(), buffer.end(), std::ostream_iterator<int>(ss1, ","));
                   std::cout << ss1.str() << std::endl;
                   */
                   
                   while(iter != this->_optionalTLVList.end())
                   {
                     struct LLDPTLV s = *iter;
                      
                     buffer.push_back(s.type);
                     buffer.push_back(s.length);
                     std::vector<uint8_t>::iterator  it3=buffer.end();
                     buffer.insert(it3, s.value.begin(), s.value.end());
              
                     //memcpy(buffer+len,&s,s.length);
                     //len+=s.length;
                     iter++;
		     
                   }
                   
                   //two complementary data.
                    
                   //int stanfor1 []={0xfe, 0x14,0x00,0x26, 0xe1,0x00, 0x6f,0x70,0x65,0x6e, 0x66,0x6c,0x6F,0x77,0x3a,0x31,0x3a, 0x4c,0x4F,0x43, 0x41,0x4C  }
                   //int standfor2 []={0xfe, 0x14,0x00,0x26,0xe1,0x01,0xc4,0x9a, 0x58,0x26, 0x0e, 0x08, 0x7f, 0xbf, 0x0e, 0x47,0xe4,0xf5,0xc1,0x1c, 0xd0,0xac,0x00,0x00 };
                   
                   // end of LLDP
                   buffer.push_back(0);
                   buffer.push_back(0);
		  return buffer; 
		   
	}

uint16_t LLDP::getLength()
{
        uint16_t len= 2+this->_chassisId.length+2+this->_portId.length+2+this->_ttl.length;
        std::list<LLDPTLV>::iterator iter = this->_optionalTLVList.begin();
        while(iter != this->_optionalTLVList.end())
           {
            struct LLDPTLV s =*iter;
	    len+=2+s.length;
            iter++;
            
	   }
	return len;
}

bool LLDP::unpack(uint8_t *buffer)
{
     /*   
      uint8_t type=(uint8_t)*((uint8_t *)(buffer));
      if(type==1) {
         this->_chassisId.type=type;
         this->_chassisId.length= (uint8_t)*((uint8_t *)(buffer+1));     
         if (this->_chassisId.length==7)
          {
            uint8_t subtype=(uint8_t)*((uint8_t *)(buffer+2)); 
            if (this->_chassisId.length==1)
           this->_chassisId.value[0]=(uint8_t)(*(uint8_t *)(buffer+2));
        else if (this->_chassisId.length==2)
           memcpy(&(this->_chassisId.value),(uint16_t *)(buffer+2),2);
        else if (this->_chassisId.length==4)	
           memcpy(&(this->_chassisId.value), (uint32_t *)(buffer+2),4);
        else if (this->_chassisId.length==8)	
           memcpy(&(this->_chassisId.value),(uint64_t *)(buffer+2),8);	
          
          
        this->_portId.type= (uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length));
        this->_portId.length=(uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+1));
		  
        if (this->_portId.length==1)
            this->_portId.value[0] =(uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+2));
        else if (this->_portId.length==2)
            memcpy(&(this->_portId.value),(uint16_t *)(buffer+2+this->_chassisId.length+2),2);
        else if (this->_portId.length==4)	
            memcpy(&(this->_portId.value),(uint32_t *)(buffer+2+this->_chassisId.length+2),4);
        else if (this->_portId.length==8)	
            memcpy(&(this->_portId.value),(uint64_t *)(buffer+2+this->_chassisId.length+2),8);

        this->_ttl.type=(uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length));
        this->_ttl.length= (uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+1));
		  
         if (this->_ttl.length==1)
	     this->_ttl.value [0]= (uint8_t)(*(uint8_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+2));
         else if (this->_ttl.length==2)
	    memcpy(&(this->_ttl.value),(uint16_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+2),2);
         else if (this->_ttl.length==4)	
            memcpy(&(this->_ttl.value),(uint32_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+2),4);
         else if (this->_ttl.length==8)	
            memcpy(&(this->_ttl.value),(uint64_t *)(buffer+2+this->_chassisId.length+2+this->_portId.length+2),8);			   
        */
		  
       return 1;
     
 }

}
