#ifndef TOPOLOGY_MANAGER_
#define TOPOLOGY_MANAGER_
 


#include "lldp.hh"
#include "topology.hh"
#include <fluid/of10msg.hh>
#include <array>
#include "core/future-util.hh"
#include "core/queue.hh"
#include "core/temporary_buffer.hh"
#include "../util/wrap_msg.hh"
#include <memory>



namespace ofd{
using namespace seastar;

class of_server;



class topology_manager

{
   
  const uint16_t LLDP_EtherType= 0x88CC; 
  
  const unsigned char TLV_DIRECTION_TYPE = 0x73;   
  const int8_t TLV_DIRECTION_LENGTH = 1; // 1 byte
  const unsigned char TLV_DIRECTION_VALUE_FORWARD = 0x01 ;
  const unsigned char TLV_DIRECTION_VALUE_REVERSE = 0x02 ;
  const unsigned char LLDP_BSN_DST_MAC_STRING [6] = {0xff,0xff,0xff,0xff,0xff,0xff};
  const unsigned char LLDP_STANDARD_DST_MAC_STRING [6] = {0x01,0x80,0xc2,0x00,0x00,0x0e};
  of_server& _server;
   

    LLDPTLV controller_tlv;
  
     
	
    public:         
     topology_manager ();     
     
     topology_manager( of_server& server ) : _server(server) {}
     
   
     of10::PacketOut packetOut_generation(node _node, uint16_t port_id,bool isStandard, bool isReverse);
     bool   packetOut_forward(node outgoing_node, OFMsg ofmsg);
     void packetIn_process(uint64_t  incoming_node, uint32_t connection_id, of10::PacketIn _packetIn);
     static bool switch_up_event(node add_node);
     static bool switch_down_event(node del_node);
     std::vector<std::unique_ptr<of10::PacketOut>>  feature_response_process ( of10::FeaturesReply _featuresReply);
     //std::vector<int> feature_response_process ( of10::FeaturesReply _featuresReply);
     static bool mulitpart_response_process(OFMsg ofmsg);
     static future<> of_portstatus( uint64_t dpid, std::unique_ptr<of10::PortStatus> ps);     	


     future<> process_Msg();
     void     handle_LLDPMsg(LLDP lldp, node incoming_node, uint16_t port_id);
     future<> read_queue_msg(wrap_msg fr);	
     future<> read();
     future<> verify(); 
     future<> validate_topology(); 
            


};
}

#endif /*TOPOLOGY_MANAGER_ */
