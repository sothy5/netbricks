#ifndef PROTOCOLS_H
#define PROTOCOLS_H


// Definition of index constants of protocol array
#define DATALINK               0
#define LAYER_2                1
#define LAYER_3                2
#define LAYER_4                3

/**
  * Enum off all implemented protocols
  */
enum protocols {
    LLDP_PROTOCOL     = 0x00001000,
    CDP_PROTOCOL      = 0x00001001,
    LLC_PROTOCOL      = 0x00001002
};

#endif // PROTOCOLS_H
