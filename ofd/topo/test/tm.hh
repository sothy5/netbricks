#ifndef TM_
#define TM_



#include "../lldp.hh"
#include "../topology.hh"
#include <fluid/of10msg.hh>
#include <array>
#include "core/future-util.hh"
#include "core/queue.hh"
#include "core/temporary_buffer.hh"
#include "../../util/wrap_msg.hh"
#include <memory>



namespace ofd{
using namespace seastar;




class tm

{

  const uint16_t LLDP_EtherType= 0x88CC;

  const unsigned char TLV_DIRECTION_TYPE = 0x73;
  const int8_t TLV_DIRECTION_LENGTH = 1; // 1 byte
  const unsigned char TLV_DIRECTION_VALUE_FORWARD = 0x01 ;
  const unsigned char TLV_DIRECTION_VALUE_REVERSE = 0x02 ;
  const unsigned char LLDP_BSN_DST_MAC_STRING [6] = {0xff,0xff,0xff,0xff,0xff,0xff};
  const unsigned char LLDP_STANDARD_DST_MAC_STRING [6] = {0x01,0x80,0xc2,0x00,0x00,0x0e};
  LLDPTLV controller_tlv;



    public:
     tm (){} ;

     

     of10::PacketOut packetOut_generation(node _node, uint16_t port_id,bool isStandard, bool isReverse);
     bool   packetOut_forward(node outgoing_node, OFMsg ofmsg);
     //static void packetIn_process(node incoming_node, OFMsg ofmsg);
     //static bool switch_up_event(node add_node);
     //static bool switch_down_event(node del_node);
     std::vector<std::unique_ptr<of10::PacketOut>>  feature_response_process ( of10::FeaturesReply _featuresReply);
     //std::vector<int> feature_response_process ( of10::FeaturesReply _featuresReply);
     //static bool mulitpart_response_process(OFMsg ofmsg);
     //static future<> of_portstatus( uint64_t dpid, std::unique_ptr<of10::PortStatus> ps);
 };


}
#endif /*TOPOLOGY_MANAGER_ */


 
