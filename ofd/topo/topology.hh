#ifndef OFD_TOPOLOGY_HH_
#define OFD_TOPOLOGY_HH_

#include <iostream>
#include <fluid/of10msg.hh>
#include <fluid/of13msg.hh>
#include <fluid/of10/of10common.hh>
#include <fluid/of13/of13common.hh>
#include <array>
#include <unordered_map>
#include <string>
#include <cstring>

namespace ofd{

using namespace fluid_msg;


class node {
private:    
	 uint64_t _node_id;
	 std::array<char,6> _addr;
	 std::vector<of10::Port> _ports;
	 std::vector<of13::Port> _ports13;
public:
         node()
         {
           this->_node_id=0;
           
         }         

	 node(uint64_t _nid,std::vector<of10::Port> ports ):_node_id(_nid),_ports(ports)
	 {
	 }
	 
         uint64_t node_id(){
	    return this->_node_id;
	 }
         
         void node_id(uint64_t nid)
         {
           this->_node_id=nid;
         }
         void ports(std::vector<of10::Port> ports)
         {
           this->_ports=ports;
         }
        
        std::vector<of10::Port> ports()
        {
           return this->_ports;
        }          
        

         EthAddress getPortHWAddress(uint16_t _port_id)
         {
	     EthAddress _ethAddress;
               for (std::vector<of10::Port>::iterator it = _ports.begin() ; it != _ports.end(); ++it)
                  if(it->port_no()== _port_id)
			 _ethAddress=it->hw_addr();
            return _ethAddress;
	}

         bool operator==(const node &other) const 
        {
           return (*this == other);
        }
         
         bool operator!=(const node &other)const
         {
            return!(*this==other);
         }
	 uint64_t get_node_id()
         {
            return this->_node_id;
	 }
         ~node(){}
	 
};
	 
class link
{
   uint64_t  _src_node_id;
   uint16_t  _src_node_port_id;
   uint64_t  _dst_node_id;
   uint16_t  _dst_node_port_id;
  
  public:  
   enum LinkType 
   {
   INVALID_LINK,
   DIRECT_LINK,
   MULTIHOP_LINK,
   TUNNEL
   };
   
 private:
   LinkType _linkType;  
  
  public:
   uint64_t src_node_id(){
    return this->_src_node_id;
   }
   uint16_t src_node_port_id(){
      return this->_src_node_port_id;
   }
   uint64_t dst_node_id(){
     return this->_dst_node_id;
   }
   uint16_t dst_node_port_id(){
      return this->_dst_node_port_id;
   }
   void src_node_id(uint64_t src_node_id){
      this->_src_node_id=src_node_id;
   }
   void src_node_port_id(uint16_t src_node_port_id){
      this->_src_node_port_id=src_node_port_id;
   }
   void dst_node_id(uint64_t dst_node_id){
      this->_dst_node_id=dst_node_id;
   }
   void dst_node_port_id(uint16_t dst_node_port_id){
      this->_dst_node_port_id=dst_node_port_id;
   }   

   link(uint64_t node_id, uint64_t src_port_id, uint64_t node_id_dst, uint64_t dst_port_id, LinkType _linkType):_src_node_id(node_id),_src_node_port_id(src_port_id),_dst_node_id(node_id_dst),_dst_node_port_id(dst_port_id),_linkType(_linkType){
     //object created. 
   }
    
  ~link()
  {
  }
 };


class discovered_topology{
   
  
    public:
    static std::list<node> switchPorts;
    static std::list<link> links;
	//May be used:
	//protected Map<DatapathId, Set<Link>> switchLinks;

	
   //discovered_topology()
   //{
      
   //}
  
   	
  static void add_node(node _node)
  {
     switchPorts.push_back(std::move(_node));
  }
  
  static  node get_node(uint64_t _node_id)
  {
        
        node _node;
        std::cout<< "Node id" << _node_id << std::endl;        
        if(!switchPorts.empty())
         {      
 	    
           std::list<node>::iterator iter = switchPorts.begin();
       		   
            node _node1;
            while(iter !=switchPorts.end())
            {
              std::cout<<"Node"<<std::endl;
              _node1 = *iter;
	      if (_node1.node_id()==_node_id)
               {
                 std::cout<<"Matched"<< std::endl; 
                return std::move(_node1);
	         	 
               }	
           iter++;
            
			
          }
        }
        return std::move(_node);
       

  
   }
   
  static node get_node(std::string mac_address) {
          
          node _node1;

           if(!switchPorts.empty()){
              std::list<node>::iterator iter=switchPorts.begin();
              node _node;
              while (iter !=switchPorts.end()){
                _node=*iter;
                uint64_t data_path_id=_node.node_id();
                uint8_t result[sizeof(data_path_id)];
                std::memcpy(result, &data_path_id, sizeof(data_path_id));
                
                std::stringstream iss;
                iss.str(mac_address);
                
                std::string item;
                int in_put_mac_address[6];
                int i=0;
                while(std::getline(iss, item, ':')){
                       in_put_mac_address[i]=std::stoi(item);
                       i++;
                 
                 }  

                if (result[0]==in_put_mac_address[0] && result[1]==in_put_mac_address[1] && result[2]==in_put_mac_address[2] && result[3]==in_put_mac_address[3] && result[4]==in_put_mac_address[4] && result[5]==in_put_mac_address[5])
{   
                 return std::move(_node);
               } 
             iter++;  
           }
  }
  return _node1; 
 
}
  static void add_port(node *_node, uint8_t portid, uint64_t mac_addr)
	{
	}
  
  static void add_link(uint64_t node_id, uint16_t port_id, uint64_t node_id_dst, uint16_t dst_node_port_id, link::LinkType _linkType )
	{
	   link _link(node_id, port_id,node_id_dst, dst_node_port_id, _linkType);
           if (!check_link(_link))
               links.push_back(std::move(_link));
  	   
	}
  static void add_link(link _link){
              if(!check_link(_link))
                 links.push_back(std::move(_link));
    }


static bool check_link(link is_new_link)
	{
	   std::list<link>::iterator iter = links.begin();
		   
           while(iter != links.end())
           {
            link _link = *iter;
			if (_link.src_node_id()==is_new_link.src_node_id() && _link.src_node_port_id()==is_new_link.src_node_port_id() && _link.dst_node_id()==is_new_link.dst_node_id() && _link.dst_node_port_id()==is_new_link.dst_node_port_id() )
                return true;
			
            iter++;
			
          }
		  
		  return false;
	}
    static bool delete_port();
	
	
    static bool delete_node();

	
    static bool delete_link();

	

};

}

#endif /* OFD_TOPOLOGY_HH */





  	




  
 




