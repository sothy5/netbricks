#include "packet.hh"

/** Returns data of which is this packet made out.
  * @return Data of this packet.
  */
const Data Packet::getData() {
    return data;
}

/** Appends data to packet.
  * @return newData Data to be appended.
  */
void Packet::appendData(Data newData) {
    data.appendData(newData);
}
