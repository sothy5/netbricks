#include <map>
#include <string>
#include "data.hh"
#include "tlv.hh"
#include <iostream>

using namespace std;

/**
  * Default type name-
  */
const string TLV::tlv_type_str = "(unknown)";

/**
  * Just definition of reference.
  */
const map<int, string> TLV::subtypes_str = map<int, string>();

/**
  * Constructor
  * @param data Data which will be used as a value.
  * @param default_subtype Subtype which specifies data value.
  */
TLV::TLV(const Data &data, int default_subtype):tlv_type(-1) , tlv_subType(default_subtype){
    setValue(data, default_subtype);
}

/**
  * Returns stored value.
  * @return Stored value in TLV.
  */
const Data TLV::getValue() {
    return tlv_value;
}

/**
  * Returns stored value as a string.
  * @return Stored value in TLV converted to string representation.
  */
const string TLV::getValueStr() {
    // fixes inserting extra \0 whether already presents
    if (tlv_value.length && tlv_value.data[tlv_value.length - 1] == '\0') {
         string s((char *)tlv_value.data);
         std::cout<<"String" << s << std::endl;
         return s;
    } else {
        
        string s((char *)tlv_value.data, tlv_value.length);
        std::cout<<"string" << s << std::endl;
        return s;
    }
}

/**
  * Returns type name.
  * @return Type name.
  */
string TLV::getTypeName() {
    return tlv_type_str;
}

/**
  * Returns subtype name of current set subtype.
  * @return Subtype name.
  */
string TLV::getSubTypeName() {
    return string();
}

/**
  * Returns current subtype.
  * @return Current subtype.
  */
int TLV::getSubType() {
    return tlv_subType;
}

/**
  * Stores new value with or without subtype.
  * @param value New value to be stored.
  * @param subtype Subtype of value.
  */
void TLV::setValue(Data value, int subtype) {
    tlv_subType = subtype;
    tlv_value = value;
}

/**
  * Destructor
  */
TLVs::~TLVs() {
    TLVs::iterator pos;

    // all allocated TLV has to be deallocated before vector destruction
    for (pos = this->begin(); pos != this->end(); ++pos) {
        delete *pos;
    }
}
