#include "core/reactor.hh"
#include "core/app-template.hh"
#include "ofserver.hh"
#include <core/thread.hh>


namespace bpo=boost::program_options;
using namespace ofd;



int main(int argc, char** argv) {
    app_template app;
    app.add_options()("port",bpo::value<uint16_t>()->default_value(6653),"OF server port");

    app.run_deprecated(argc, argv, [&] {
           auto&& config = app.configuration();
           uint16_t port=config["port"].as<uint16_t>();
           auto server = new of_server_control();

           std::cout << "Hello world\n";
           std::cout << "Smp::count" << smp::count << std::endl;
           
           server->start().then([server,port] {
              return server->listen(port);
              }).then([server,port]
              {  
               std::cout << "Seastar OF server listening on port " << port << ".... \n";
               engine().at_exit([server] {
                   return server-> stop();
                });

             });
             
            
    }); 

}
