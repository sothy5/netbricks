#ifndef APPS_OFSERVER_HH_
#define APPS_OFSERVER_HH_


#include "core/reactor.hh"
#include "core/sstring.hh"
#include <experimental/string_view>
#include "core/app-template.hh"
#include "core/circular_buffer.hh"
#include "core/distributed.hh"
#include "core/queue.hh"
#include "core/future-util.hh"
#include "core/scollectd.hh"
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <queue>
#include <bitset>
#include <limits>
#include <cctype>
#include <vector>
#include <boost/intrusive/list.hpp>
#include <fluid/of10msg.hh>
#include <fluid/ofcommon/msg.hh>
#include <fluid/of13msg.hh>
#include "core/thread.hh"
#include "./l2switch/l2switch.hh"
#include "./topo/topologymanager.hh"
#include "./util/wrap_msg.hh"
#include <exception>
#include <mutex>
#include <shared_mutex>
#include "./topo/test/tm.hh"
#include <cstdlib>  


using namespace fluid_msg;



#define HELLO_XID 0x0F

namespace ofd {


class learningswitch;

class of_server {
    std::vector<server_socket> _listeners;
 
    uint64_t _total_connections = 0;
    uint64_t _current_connections = 0;
    uint64_t _requests_served = 0;
    uint64_t _connections_being_accepted = 0;
    bool _stopping = false;
    promise<> _all_connections_stopped;
    future<> _stopped = _all_connections_stopped.get_future();
   
    
private:
    void maybe_idle() {
        if (_stopping && !_connections_being_accepted && !_current_connections) {
            _all_connections_stopped.set_value();
        }
    }
     

    

public:
     

     queue<wrap_msg> *_spsc_queue=new queue<wrap_msg>(128);
     queue<wrap_msg_return> * _spsc_queue_l2s_ofd=new queue<wrap_msg_return>(128);
     queue<wrap_msg> *_spsc_queue_topo=new queue<wrap_msg>(128);
     queue<wrap_msg_return> *_spsc_queue_topo_ofd=new queue<wrap_msg_return>(128);


    of_server() {
          
    }
    future<> listen(ipv4_addr addr) {
       //auto cpu=engine().cpu_id();
       //std::cout << "cpu" << cpu << std::endl;   
       listen_options lo;
       lo.reuse_address = true;
       _listeners.push_back(engine().listen(make_ipv4_address(addr), lo));
       _stopped = when_all(std::move(_stopped), do_accepts(_listeners.size() - 1)).discard_result();
        
       do_with(learningswitch(*this), [] (learningswitch& l2s) {
              return l2s.process();
       });
       
        
       do_with(topology_manager(*this), [] (topology_manager& tm) {
                  return tm.process_Msg();
       }); 
       
  
      receive_msg_from_l2s();
      receive_msg_from_topo();
      return make_ready_future<>();
    }
    
    future <> receive_msg_from_l2s()
    {
         return keep_doing([this] {
                 
             return this->_spsc_queue_l2s_ofd->pop_eventually().then( [ this] ( wrap_msg_return wm)  
               {
                  
                 for (auto it=_connections.begin(); it!=_connections.end(); it++)
                  {
                   
                      if ( it->_dpid ==wm._dpid)
                         {

                              char *data_=new char[wm._buf.size()];

                              for (uint8_t i=0; i< wm._buf.size();i++)
                                 data_[i]=wm._buf.at(i);


                              temporary_buffer<char> buf_out( data_, wm._buf.size(),deleter());
                              it->write( std::move(buf_out));
                              return  make_ready_future<>();
                              
                                
                         }
                  }
                  
                  
                 return make_ready_future<>();
               });

                
        } );
    }


    future<> receive_msg_from_topo()
    {
         return keep_doing([this] {

             return this->_spsc_queue_topo_ofd->pop_eventually().then( [ this] ( wrap_msg_return wm)
               {

                 for (auto it=_connections.begin(); it!=_connections.end(); it++)
                  {

                      if ( it->_dpid ==wm._dpid)
                         {
                             //std::cout<< "came for topop sending" << std::endl;
                             char *data_=new char[wm._buf.size()];

                             for (uint8_t i=0; i< wm._buf.size();i++)
                                 data_[i]=wm._buf.at(i);
                                   
                                 
                             temporary_buffer<char> buf_out( data_, wm._buf.size(),deleter());

                             return it->write( std::move(buf_out)).then_wrapped([it] (auto f){
                                if (f.failed()) {
                                    auto e = f.get_exception();
                                    std::cerr << " failed tcp connectionrequest error" << e << std::endl;
                                    delete &it;
                                    return make_ready_future<>();                                
                                  } else {
                                    return make_ready_future<>();
                                 }
                             });
                             

                         }
                  }

                 return make_ready_future<>();
               });


        } );
    }


    future<> stop() {
        _stopping = true;
        for (auto&& l : _listeners) {
            l.abort_accept();
        }
        for (auto&& c : _connections) {
            c.shutdown();
        }
        
     
     delete _spsc_queue;
     delete  _spsc_queue_l2s_ofd;
     delete  _spsc_queue_topo;
     delete  _spsc_queue_topo_ofd;    
        

        return std::move(_stopped);
    }
    
 future<> do_accepts(int which) {
        ++_connections_being_accepted;
        return _listeners[which].accept().then_wrapped(
                [this, which] (future<connected_socket, socket_address> f_cs_sa) mutable {
                    --_connections_being_accepted;
                    if (_stopping) {
                        maybe_idle();
                        return;
                    }
                    auto cs_sa = f_cs_sa.get();
                    auto conn = new OFconnection(*this, std::get<0>(std::move(cs_sa)), std::get<1>(std::move(cs_sa)));
                    conn->process().then_wrapped([this, conn] (auto&& f) {
                                delete conn;
                                try {
                                    f.get();
                                } catch (std::exception& ex) {
                                    std::cerr << "request error " << ex.what() << std::endl;
                                }
                            });
                    do_accepts(which);
                }).then_wrapped([] (auto f) {
            try {
                f.get();
            } catch (std::exception& ex) {
                std::cerr << "accept failed: " << ex.what() << std::endl;
            }
        });
     }



class OFconnection  :public boost::intrusive::list_base_hook<> {

public:
         enum state
              {
                  STATE_HANDSHAKE,
                  STATE_RUNNING,
                  STATE_FAILED,
                  STATE_DOWN,
              };
          enum event
          {
                EVENT_STARTED,
                EVENT_ESTABLISHED,
                EVENT_FAILED_NEGOTIATION,
                EVENT_CLOSED,
                EVENT_DEAD,
          };
   
private:   
        of_server& _server;
        connected_socket _fd;
        input_stream<char> _read_buf;
        output_stream<char> _write_buf;
        using tmp_buf = temporary_buffer<char>;
        void* application_data; 
        std::unique_ptr<of10::FeaturesReply> _req;
        std::unique_ptr<of10::PacketOut> _resp;
        queue<std::unique_ptr<of10::PacketOut>> _replies { 10 };
        bool _done = false;
        uint8_t _max_suported_version=1 ;
        state _state;
   
public: 
     uint64_t _dpid;
     uint32_t  _connection_id=0;  
   
         
 
        void set_state(state st)
        {
           this->_state=st;
        }

        OFconnection(of_server& server ,connected_socket&& fd,
                socket_address addr)
                : _server(server),_fd(std::move(fd)), _read_buf(_fd.input()), _write_buf(
                        _fd.output()),_state(STATE_HANDSHAKE) {
            ++_server._total_connections;
            ++_server._current_connections;
            _server._connections.push_back(*this);
           _connection_id=rand();  
           //auto cpu=engine().cpu_id();
           //std::cout << "cpu count in connection" << cpu << std::endl; 
           //std::cout << _server._total_connections << std::endl;
           //std::cout << _server._current_connections << std::endl;        
            
           
               
        }
        ~OFconnection() {
                
            --_server._current_connections;
            std::cout<<"Connection disconnected" << _dpid<< std::endl;
            _server._connections.erase(_server._connections.iterator_to(*this));
            _server.maybe_idle();
           set_state(STATE_DOWN);
                
        }

        future<> process() {
          return when_all(read(),respond()).then(
                    [] (std::tuple<future<>, future<>> joined) {
                        // FIXME: notify any exceptions in joined?
                         //std::cout << "process finished" << std::endl;
                         
                        return make_ready_future<>();
                    });

        }
        void shutdown() {
            _fd.shutdown_input();
            _fd.shutdown_output();
        }

        future<> read() {
             //[this] {return _done;},
            return do_until( [this] { return _done; }, [this] {
                //std::cout << "first call" << std::endl;
                return read_one();
            }).then_wrapped([this] (future<> f) {
                // swallow error
              // FIXME: count it?
                 std::cout<< "Error or exception"<< std::endl;
		//return make_ready_future<>(); 
                // _replies.push_eventually( {});
            }).finally([this] {
                
                std::cout << "Read buffer is closed" << std::endl;;
                return _read_buf.close();
            });
        
        }
        
        future<> respond()
        {

            OFMsg* ofmsg=new OFMsg(_max_suported_version,fluid_msg::of10::OFPT_HELLO,htons(HELLO_XID));
            temporary_buffer<char> buf((char*)ofmsg->pack(),ofmsg->length(),deleter());
            _write_buf.write(std::move(buf)).then([this ]{
                       //std::cout<< "wrote  first HELLO message" << std::endl;
                       _write_buf.flush();     
             });
           delete ofmsg;
           return make_ready_future<>();
  
        }

         future<> do_response_loop() {
            return _replies.pop_eventually().then(
                    [this] (std::unique_ptr<of10::PacketOut> resp) {
                         
                               
                            of10::PacketOut *po=resp.get();
                            temporary_buffer<char> buf_out1((char *)po->pack(),po->length(),deleter());
                            std::cout<<"Length"<< resp->length() << std::endl;
                            _write_buf.write(std::move(buf_out1)).then( [this] {
                            std::cout <<"PACKET OUT there" << std::endl;                                  
                                     _write_buf.flush();
                              
                            });
                    });
              }
			 

        future<> read_one() {
         
            
           if (!_read_buf.eof()) 
           {   
               return _read_buf.read().then_wrapped([this] (auto f) {
             
   
                 if (f.failed())
                  {
                    auto e = f.get_exception();
                    std::cerr << "Request error read_one" << e << std::endl;
                    _done=true;
                    return make_exception_future<>(std::move(e));     
                  }
                 else 
                  {

                       auto buf1=f.get0();
                       return this->proces_read_msg(std::move(buf1));
                  }
                 });
         
         }else
         {
            std::cout<<"New error detection mechansim" << std::endl;
            //_read_buf.close();
            _done=true;
           return make_ready_future<>();
         }
        }
                
      future<> proces_read_msg( temporary_buffer<char>  buf )
       {
            
               if (buf) {
         
                       uint16_t of_version = (uint16_t)((buf.get())[0]); 
                      // std::cout<< "msg_version " << of_version << std::endl;
                       uint16_t of_type= (uint16_t)((buf.get())[1]);
                      // std::cout << "msg_type"<< of_type << std::endl;                    
                          
                         if(of_type == fluid_msg::of10::OFPT_HELLO) 
                          {
                     

                           if(of_version == 4) 
                            {
                              this->install_default_flow13().then([]{
                                   make_ready_future<>();
                              });

                            }                        

                       
                            if ( of_version == 4 || of_version == 1)
                            {
                               _max_suported_version= of_version;
                               char a=0x05;
                               (buf.get_write())[1] =a;
                               // std::cout << "Feature request made" << std::endl;
                               // std::cout << "version negotiation " << of_version << std::endl;
                              // std::cout<< "of hello made and feature request is made" << std::endl; 
                               _write_buf.write(std::move(buf)).then([this]{
                                       // _write_buf.flush();
                                       // std::cout << "wrote data"<< std::endl;
                                       // make_ready_future<>();
                                       _write_buf.flush();
                                       });
                            }                                          
                         
                         }
                         else if (of_type==fluid_msg::of10::OFPT_FEATURES_REPLY)
                         {
                        
                            of10::FeaturesReply featuresReply;
                            featuresReply.unpack((uint8_t *)const_cast<char*>(buf.get()));
                            this->_dpid=featuresReply.datapath_id();
                            std::cout << "DPID" << std::endl;

                              if(of_version ==1)
                              { 
                                 std::vector<uint8_t> data(buf.get(), buf.get()+buf.size());
                                 wrap_msg wm(this->_dpid,_connection_id ,of_type, data);
                                 _server._spsc_queue_topo->push_eventually(std::move(wm)).handle_exception( []  (std::exception_ptr e) { 
                                   std::cerr << e << std::endl;
                                 
                                  });
                                 data.clear();
                                   
                                return  install_arp().then([]{
                                  return make_ready_future<>();
                                 });
                                 
                                    
                             } 
          
                         } 
                         else if (of_type==fluid_msg::of10::OFPT_ECHO_REQUEST)
                         { 
                                char a=0x03;
                                (buf.get_write())[1] =a; 
                               // std::cout<<"New Data ECHO REPLY \n";
                                
                                this->_write_buf.write(std::move(buf)).then([this]{
                                       // make_ready_future<>();
                                        _write_buf.flush();
                                       });
                                //_server._spsc_queue->push_eventually(of_type);


                         }   
                         else if (of_type==fluid_msg::of10::OFPT_PACKET_IN)
                         {
                             if(of_version ==1)
                              {
                                std::cout<< "PacketIn arrived" << std::endl;
                                std::vector<uint8_t> data(buf.get(), buf.get()+buf.size());
                                of10::PacketIn ofpi;
                                ofpi.unpack((uint8_t *)const_cast<char*>(buf.get()));
                                uint8_t* data_= (uint8_t *) ofpi.data();
                             
                                 if ( data_[0]==0x01 && data_[1]==0x23 && data_[2]==0x00 && data_[3]==0x00 && data_[4]==0x00 && data_[5]==0x01){ 
                                                      
                                       std::cout << "Packet IN for topology" << of_type << std::endl;
                                       wrap_msg wm(this->_dpid,_connection_id ,of_type, data);
                                       _server._spsc_queue_topo->push_eventually(std::move(wm)).handle_exception( []  (std::exception_ptr e) {
                                       std::cerr << e << std::endl;
                                       });
                                     
                                    
                                    
                                 }else{
                                 
                                   if(this->_dpid!=0) { 
                                   wrap_msg wm(this->_dpid,_connection_id,of_type, data);
                                   //std::cout <<"DPID" << this->_dpid << std::endl;
                                   _server._spsc_queue->push_eventually(std::move(wm)).handle_exception( []  (std::exception_ptr e) { 
                                     std::cerr << e << std::endl;
                              
                                    }); 
                                   }

                                 }  
                             
                             }else if(of_version ==4)
                             {
                               of13::PacketIn* ofpi_3=new of13::PacketIn();
                               ofpi_3->unpack((uint8_t *)const_cast<char*>(buf.get()));
                                 
                         
                             }  
                         

                     
                        }else if (of_type==fluid_msg::of10::OFPT_PORT_STATUS )
                           {
                              if(of_version==1)
                              {
                                 //std::cout<< "port status  arrived" << std::endl;
                                 of10::PortStatus ofps;
                                 ofps.unpack((uint8_t *)const_cast<char *>(buf.get()));
                                 //topology_manager::of_portstatus( dpid,std::make_unique<of10::PortStatus> (ofps));
                              }  
     

                          }else if( of_type==fluid_msg::of10::OFPT_ERROR)
                          {
                             if(of_version==1)
                             {
                              std::cout<< "Type" <<(uint8_t) (buf.get())[6]  << std::endl;
                              std::cout<< "Code" << (uint8_t) (buf.get())[8] << std::endl;
                             }   

                         }
                         else {
                           std::cout<< "Un processed Type"<< of_type<< std::endl;
                         } 
                    
                     return make_ready_future<>();
                    

                 }else 
                 {
                    //std::cout << "EOF\n";
                    return make_ready_future<>();
                 }
                
           //});
         }
         
          future<> write( temporary_buffer<char> buf_out)
          {
                 return _write_buf.write(std::move(buf_out)).then_wrapped([this] (auto f){
                                if (f.failed()) {
                                    auto e = f.get_exception();
                                    std::cerr << "request error in connection" << e << std::endl;
                                    this->_write_buf.close();
                                    this->_read_buf.close();
                                    return make_exception_future<>(std::move(e));                                
                                  } else {
                                     //std::cout<<"wrote data" << std::endl;
                                     return _write_buf.flush();
                                    //return make_ready_future<>();
                                 }
                   });
               
              

          }  
         
           future<bool> generate_reply ( std::unique_ptr<of10::FeaturesReply> req ){
                 auto resp=std::make_unique<of10::PacketOut>();
                 return make_ready_future<bool>(true);  
			
		    
         }          
          
         future<> install_arp(){
              of10::FlowMod fm(42,123,of10::OFPFC_ADD,5,10,100,0xffffffff,0,0);
              of10::Match m;
              m.dl_type(0x0806);
              fm.match(m);
              of10::OutputAction act(of10::OFPP_FLOOD,1024);
              fm.add_action(act); 
              temporary_buffer<char> buf_out((char *)fm.pack(),fm.length(),deleter());
              return this->_write_buf.write(std::move(buf_out)).then([] {
                  std::cout<<"Sedning ARP"<< std::endl;
                  return make_ready_future<>();
              });     
         
         }
               
          future<> install_lldp(of10::PacketOut po)
          {
               return make_ready_future<>(); 
          }
         
         future<> install_default_flow13()
          {
           of13::FlowMod fm(42,0,0xffffffffffffffff,0,of13::OFPFC_ADD,0,0,0,0xffffffff,0,0,0);
           of13::OutputAction *act=new of13::OutputAction(of13::OFPP_CONTROLLER,of13::OFPCML_NO_BUFFER);
           of13::ApplyActions *inst=new of13::ApplyActions();
           inst->add_action(act);
           fm.add_instruction(inst);
           temporary_buffer<char> buf_out((char *)fm.pack(),fm.length(),deleter());
           return this->_write_buf.write(std::move(buf_out)).then([]{
                                 return  make_ready_future<>();
                             });

          }              
        

       }; 
       uint64_t total_connections() const {
        return _total_connections;
       }
       uint64_t current_connections() const {
          return _current_connections;
       }
       uint64_t requests_served() const {
          return _requests_served;
       }
private:
    boost::intrusive::list<OFconnection> _connections;

     
};

class of_server_control {
    distributed<of_server>* _server_dist;
public:
    of_server_control() : _server_dist(new distributed<of_server>) {
    }

    future<> start() {
        return _server_dist->start();
    }

    future<> stop() {
        return _server_dist->stop();
    }

    /*
    future<> set_routes(std::function<void(routes& r)> fun) {
        return _server_dist->invoke_on_all([fun](of_server& server) {
                fun(server._routes);
        });
    }
     */

    future<> listen(ipv4_addr addr) {
        return _server_dist->invoke_on_all(&of_server::listen, addr);
    }

    distributed<of_server>& server() {
        return *_server_dist;
    }
};


}
#endif /* APPS_OFSERVER_HH_ */


