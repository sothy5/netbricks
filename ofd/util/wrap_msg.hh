#ifndef APPS_MSG_HH_
#define APPS_MSG_HH_

#include <fluid/of10msg.hh>
/*
#include "core/temporary_buffer.hh"
*/

using namespace fluid_msg;

namespace ofd {
         struct wrap_msg{
            uint64_t _dpid;
            uint32_t _connection_id;
            uint8_t _msgType;
            std::vector<uint8_t> _buf={};
            
            //of10::PacketIn  _ofmsg;
            //uint8_t* _tmp;
           // temporary_buffer<char> _buf;             
            //uint16_t _length;
            
           //wrap_msg( uint64_t dpid, of10::PacketIn ofmsg, uint16_t len ):_dpid(dpid), _ofmsg( std::move(ofmsg)), _length(len) {}
            wrap_msg(uint64_t dpid, uint64_t connection_id, uint8_t msgType, std::vector<uint8_t> buf): _dpid(dpid),_connection_id(connection_id) , _msgType(msgType), _buf( std::move(buf)) {}; 
           // dpid(uint64_t id) { _dpid=id ;}
           //dpid() { return _dpid;}
           //ofmsg(OFMsg om){_ofmsg=om; }
           //ofmsg(){ return _ofmsg;}
              
         };
       struct wrap_msg_return{
             uint64_t _dpid;
             //uint64_t _connection_id;
             uint8_t _msgType;
             //uint8_t* _tmp;
             //char -> uint8_t
             std::vector<uint8_t> _buf;
             uint16_t _length;
             wrap_msg_return():_dpid(0), _msgType(0),_length(0) {};
             wrap_msg_return(uint64_t dpid): _dpid(dpid), _msgType(0),_length(0) {};
             wrap_msg_return(uint64_t dpid, uint8_t msgType, std::vector<uint8_t> buf): _dpid(dpid),  _msgType(msgType), _buf(buf), _length(0){}; 
      };

 
}
#endif /* APPS_MSG_HH_ */
