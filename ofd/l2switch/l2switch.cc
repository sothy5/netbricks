#include "../ofserver.hh"
#include <iostream>
#include "l2switch.hh"

namespace ofd
{

GL2TABLE learningswitch::_gL2TABLE={};

seastar::future<> learningswitch::process()
{

 return keep_doing( [this] 
        { 
         
          return this->_server._spsc_queue->pop_eventually().then_wrapped ( [this](auto f){
                     try 
                      { 
                       wrap_msg wm =f.get0();
                        
                          
                       //std::cout<< "POPPED msg"<< wm._dpid << std::endl;
                       //of10::PacketIn ofpi;
                       //ofpi.unpack(( uint8_t *) const_cast<char*>( wm._buf.get()));
                       // std::cout << "of version" << wm._ofmsg.in_port() << std::endl;
      
                       
                       return this->process_packet_in( std::move(wm)).then([]() {
                         //std::cout << "Packetin processed"<< std::endl;
                              return make_ready_future<>();
                       });
                        
                       
                        return make_ready_future<>(); 
                          
                      }catch(...)
                       {
                        std::cerr << "request error" << std::current_exception() << std::endl;
                        return make_ready_future<>();
   
                       } 
                         
           });  
          
     
       }); 
 
}

seastar::future<> learningswitch::process_packet_in( wrap_msg wm  )
{
  
       //std::cout<< "Thread printing"<< wm._dpid << std::endl;
       
       L2TABLE* _l2table=new L2TABLE();
      
       if(_gL2TABLE.size()==0)
       {
        
        _gL2TABLE.insert({wm._dpid, _l2table});
       }
       else
        {
           try{   
             _l2table=_gL2TABLE.at(wm._dpid);
              //return learning_logic(a._dpid,a._ofmsg,std::move(_up_l2table)).then( [this] ( wrap_msg_return wmr) {
              //      return this->_server._spsc_queue_l2s_ofd->push_eventually(std::move(wmr));

              // }); 
             
           }catch(const std::out_of_range& oor  ){
                    _gL2TABLE.insert({wm._dpid, _l2table});

                     
                   } 
       }


       return learning_logic(std::move(wm), _l2table).then( [this] ( wrap_msg_return wmr) {
                    
                    if(wmr._dpid>0)
                    {
                       this->_server._spsc_queue_l2s_ofd->push_eventually(std::move(wmr));
                    }
                    return make_ready_future<>();
               });


}

seastar::future<wrap_msg_return> learningswitch::learning_logic(wrap_msg wm,  L2TABLE* l2table ) {
       
      
       uint64_t dst=0,src=0;
       uint16_t in_port=0;
      

       wrap_msg_return wmr( wm._dpid) ;

        
      of10::PacketIn ofpi;
      ofpi.unpack((uint8_t *)( wm._buf.data())) ;
      uint8_t ver=ofpi.version();
      //std::cout<< "version" << ofpi.in_port() << std::endl; 
               
       if (ofpi.version() == of10::OFP_VERSION) {
                 //data = (uint8_t*) ofpi->data();
                 memcpy(((uint8_t*) &dst) + 2, (uint8_t*) ofpi.data(), 6);
                 memcpy(((uint8_t*) &src) + 2, (uint8_t*) ofpi.data() + 6, 6);
                 in_port = ofpi.in_port();
       }   
       
        
          l2table->insert({src, in_port});

          L2TABLE::iterator it = l2table->find(dst);
          if (it == l2table->end()) {
                if (ver == of10::OFP_VERSION) {
                      of10::PacketOut po= flood10(ofpi);
                      uint8_t* tmp=(uint8_t *) po.pack();
                      std::vector<uint8_t> vec(tmp, tmp+po.length());
                      wrap_msg_return _wmr(wm._dpid,of10::OFPT_PACKET_IN, vec);
                      return make_ready_future<wrap_msg_return>(std::move(_wmr));
                            
                }
                
           }
          

          if (ver == of10::OFP_VERSION) {
                 of10::FlowMod fm= install_flow_mod10(ofpi, src,dst,it->second); 
                 uint8_t* tmp=(uint8_t *) fm.pack();
                 std::vector<uint8_t> vec(tmp, tmp+fm.length());
                 wrap_msg_return _wmr(wm._dpid, of10::OFPT_FLOW_MOD,vec );
                 return make_ready_future<wrap_msg_return>(_wmr);
                       
          }
          
     return make_ready_future<wrap_msg_return>(wmr);    
   
}

of10::PacketOut   learningswitch::flood10(of10::PacketIn &pi){
       of10::PacketOut po(pi.xid(), pi.buffer_id(), pi.in_port());
       if (pi.buffer_id() == (uint32_t)-1) {
           po.data(pi.data(), pi.data_len());
       }
        of10::OutputAction act(of10::OFPP_FLOOD, 1024);
        po.add_action(act);
       return po;
     
}
of10::FlowMod learningswitch::install_flow_mod10(of10::PacketIn &pi, uint64_t src, uint64_t dst, uint16_t out_port)
{
     of10::FlowMod fm(pi.xid(),123,of10::OFPFC_ADD,5,10,100,pi.buffer_id(),0,0);
     of10::Match m;
     m.dl_src(((uint8_t*) &src) + 2);
     m.dl_dst(((uint8_t*) &dst) + 2);
     fm.match(m);
     of10::OutputAction act(out_port, 1024);
     fm.add_action(act);
     return fm;

}


}
