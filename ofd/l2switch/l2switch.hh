
#ifndef OFD_L2SWITCH_HH
#define OFD_L2SWITCH_HH

#include <iostream>

/* 
#include "core/future-util.hh"
*/

#include "../util/wrap_msg.hh"
#include <unordered_map> 
#include "core/future.hh"


namespace ofd {

class of_server;

typedef std::unordered_map<uint64_t, uint16_t>  L2TABLE;
typedef std::unordered_map<uint64_t, L2TABLE*> GL2TABLE;

using namespace seastar;

class learningswitch {
   of_server& _server;
   bool _done=false;
   
  public:
   
   static GL2TABLE _gL2TABLE;
   learningswitch(of_server& server):_server(server) {} 
   future<> process();
    future<> process_packet_in(wrap_msg wm); 
  private:
  future<wrap_msg_return> learning_logic(wrap_msg wm, L2TABLE* l2table);
  of10::PacketOut flood10(of10::PacketIn & pin);
  of10::FlowMod install_flow_mod10(of10::PacketIn & pin, uint64_t src, uint64_t dst, uint16_t outport);   
   


};

}


#endif /* OFD_L2SWITCH_HH */
